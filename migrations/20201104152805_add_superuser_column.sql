-- Add migration script here

ALTER TABLE admins
    ADD COLUMN IF NOT EXISTS is_superuser bool;
