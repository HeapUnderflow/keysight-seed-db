-- Add migration script here

DROP TABLE IF EXISTS admins;

ALTER TABLE users
    ADD COLUMN IF NOT EXISTS flags smallint NOT NULL DEFAULT 1;
