ALTER TABLE users
    ALTER COLUMN flags DROP DEFAULT,
    -- this hacky int2-int4 conversion is needed to make postgres happily convert to bit string
    -- it basically forces it to i16, then to i32, and then it truncates to a 16 bit bitstring
    ALTER COLUMN flags TYPE bit(16) USING (flags::int2)::int4::bit(16),
    ALTER COLUMN flags SET DEFAULT B'0000000000000001';
