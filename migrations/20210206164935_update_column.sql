-- Add migration script here

ALTER TABLE seeds
    ADD COLUMN updated TIMESTAMPTZ NOT NULL DEFAULT now();

-- noinspection SqlWithoutWhere
UPDATE seeds SET updated = created;

-- Update Trigger Function (sets updated to now)
CREATE OR REPLACE FUNCTION trigger_set_updated_at()
    RETURNS TRIGGER AS $$
    BEGIN
        NEW.updated = now();
        RETURN NEW;
    END;
    $$ LANGUAGE plpgsql;

CREATE TRIGGER seeds_trigger_set_updated_at
    BEFORE UPDATE ON seeds
    FOR EACH ROW
    EXECUTE PROCEDURE trigger_set_updated_at()
