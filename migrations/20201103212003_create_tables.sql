-- Add migration script here

-- Users Table

CREATE TABLE IF NOT EXISTS public.users
(
    id character(32) COLLATE pg_catalog."default" NOT NULL,
    name character(32) COLLATE pg_catalog."default" NOT NULL,
    image text COLLATE pg_catalog."default",
    CONSTRAINT pk_id PRIMARY KEY (id)
);

ALTER TABLE public.users
    OWNER to keysight_user;

-- Admin Table

CREATE TABLE IF NOT EXISTS public.admins
(
    user_id character(32) COLLATE pg_catalog."default" NOT NULL,
    is_admin boolean NOT NULL,
    CONSTRAINT admin_pk_id PRIMARY KEY (user_id),
    CONSTRAINT foreign_id FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);

ALTER TABLE public.admins
    OWNER to keysight_user;

-- Seed Table

CREATE TABLE IF NOT EXISTS public.seeds
(
    id character varying(512) COLLATE pg_catalog."default" NOT NULL,
    user_id character(32) COLLATE pg_catalog."default" NOT NULL,
    created timestamptz NOT NULL,
    image text COLLATE pg_catalog."default",
    tags character varying(64)[] COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT seed_pk_id PRIMARY KEY (id),
    CONSTRAINT fk_user_id FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);

ALTER TABLE public.seeds
    OWNER to keysight_user;
