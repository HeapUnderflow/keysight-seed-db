-- Add migration script here

ALTER TABLE admins
    DROP COLUMN IF EXISTS is_admin,
    DROP COLUMN IF EXISTS is_superuser,
    ADD COLUMN IF NOT EXISTS flags smallint NOT NULL
