// BSD 3-Clause License
//
// Copyright (c) 2020, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use serde::{Deserialize, Serialize};
use sqlx::{postgres::PgRow, Error, FromRow, Row};
use time::OffsetDateTime;

#[derive(Debug, Serialize, Deserialize)]
pub struct Tag {
	prefix: Option<String>,
	name:   String,
}

impl Tag {
	pub fn new(s: &str) -> Tag {
		let mut parts = s.splitn(2, ':');
		let one = parts.next().unwrap();
		let two = parts.next();

		match two {
			Some(two) => Tag {
				prefix: Some(one.to_owned()),
				name:   two.to_owned(),
			},
			None => Tag {
				prefix: None,
				name:   one.to_owned(),
			},
		}
	}
}

#[derive(Debug)]
pub struct Seed {
	id:      String,
	user:    User,
	created: OffsetDateTime,
	image:   Option<String>,
	tags:    Vec<Tag>,
}

impl<'r> FromRow<'r, PgRow> for Seed {
	fn from_row(row: &'r PgRow) -> Result<Self, Error> {
		Ok(Seed {
			id:      row.try_get("id")?,
			user:    User::from_row(row)?,
			created: row.try_get("created")?,
			image:   row.try_get("image")?,
			tags:    row
				.try_get::<Vec<String>, _>("tags")?
				.into_iter()
				.map(|v| Tag::new(&v))
				.collect(),
		})
	}
}

#[derive(Debug)]
enum User {
	Partial { id: String },
	Full { id: String, name: String },
}

impl<'r> FromRow<'r, PgRow> for User {
	fn from_row(row: &'r PgRow) -> Result<Self, Error> {
		let id = row.try_get::<&str, _>("user_id")?.trim_end().to_owned();
		let name = row.try_get("user_name").ok();
		match name {
			Some(name) => Ok(User::Full { id, name }),
			None => Ok(User::Partial { id }),
		}
	}
}
