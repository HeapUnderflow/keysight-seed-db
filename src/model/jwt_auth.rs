// BSD 3-Clause License
//
// Copyright (c) 2020, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use crate::{
	config::Config,
	db::Database,
	model::{UserFlags, KEY},
	page::API_AUTH_TOKEN_COOKIE,
	utils,
};
use bit_vec::BitVec;
use jsonwebtoken::{
	self as jwt,
	errors::ErrorKind,
	Algorithm,
	DecodingKey,
	EncodingKey,
	Header,
	TokenData,
	Validation,
};
use rocket::{http::Status, outcome::Outcome, request, request::FromRequest, Request};
use serde::{Deserialize, Serialize};
use std::{
	collections::HashSet,
	fmt,
	fmt::{Display, Formatter},
	str::FromStr,
};
use time::{Duration, OffsetDateTime};

const ALGORITHM: Algorithm = Algorithm::HS512;
const ISS: &str = env!("CARGO_PKG_NAME");

#[allow(non_camel_case_types)]
#[derive(Debug, Serialize, Deserialize, PartialOrd, Ord, PartialEq, Eq)]
#[serde(rename = "UPPERCASE")]
pub enum Role {
	USER,
	ADMIN,
	OWNER,
}

impl Display for Role {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::USER => write!(f, "USER"),
			Self::ADMIN => write!(f, "ADMIN"),
			Self::OWNER => write!(f, "OWNER"),
		}
	}
}

impl FromStr for Role {
	type Err = ();

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		match s.to_ascii_uppercase().as_str() {
			"USER" => Ok(Self::USER),
			"ADMIN" => Ok(Self::ADMIN),
			"OWNER" => Ok(Self::OWNER),
			_ => Err(()),
		}
	}
}

pub fn generate_token(
	key: &[u8],
	sub: String,
	aud: UserFlags,
	exp_in: Duration,
	twitch_claims: TwitchClaims,
) -> Option<String> {
	if exp_in < Duration::nanoseconds(0) {
		return None;
	}

	let mut header = Header::default();
	header.alg = ALGORITHM;
	header.cty = Some("application/json".to_owned());

	let claims = Claims {
		sub,
		aud,
		iss: ISS.to_owned(),
		iat: OffsetDateTime::now_utc(),
		exp: OffsetDateTime::now_utc() + exp_in,
		kssdb_twitch: twitch_claims,
	};

	jwt::encode(&header, &claims, &EncodingKey::from_secret(key)).ok()
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
	pub sub:          String,
	pub aud:          UserFlags,
	pub iss:          String,
	#[serde(with = "time::serde::timestamp")]
	pub iat:          OffsetDateTime,
	#[serde(with = "time::serde::timestamp")]
	pub exp:          OffsetDateTime,
	pub kssdb_twitch: TwitchClaims,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TwitchClaims {
	pub img:  Option<String>,
	pub name: String,
}

// noinspection RsTypeCheck
fn verify_and_extract(
	token: &str,
	key: &[u8],
	target_role: UserFlags,
) -> Result<Claims, AuthenticationError> {
	let mut aud = HashSet::new();
	aud.insert(target_role);

	let mut verify = Validation::default();
	verify.algorithms = vec![ALGORITHM];
	verify.validate_exp = true;
	verify.validate_nbf = false;
	verify.iss = Some(ISS.to_owned());

	verify.leeway = 0;

	jwt::decode(token, &DecodingKey::from_secret(key), &verify)
		.map(|data: TokenData<Claims>| data.claims)
		.map_err(|error| match error.kind() {
			ErrorKind::InvalidSignature => AuthenticationError::InvalidSignature,
			ErrorKind::InvalidKeyFormat => AuthenticationError::InvalidFormat,
			ErrorKind::ExpiredSignature => AuthenticationError::Expired,
			ErrorKind::InvalidIssuer => AuthenticationError::InvalidIssuer,
			ErrorKind::ImmatureSignature => AuthenticationError::IssuedInFuture,
			ErrorKind::InvalidAlgorithm => AuthenticationError::InvalidAlgorithm,
			_ => AuthenticationError::Other,
		})
		.and_then(|value| {
			if value.aud.contains(UserFlags::BANNED) {
				Err(AuthenticationError::Banned)
			} else if !value.aud.contains(target_role) && !value.aud.contains(UserFlags::OWNER) {
				Err(AuthenticationError::MissingRole)
			} else {
				Ok(value)
			}
		})
}

async fn validate(
	db: &Database,
	token: &str,
	key: &[u8],
	target_role: UserFlags,
) -> Result<Claims, AuthenticationError> {
	let valid = tokio::task::block_in_place(move || verify_and_extract(token, key, target_role));

	match valid {
		Ok(claims) => {
			match sqlx::query_file!("queries/get_user.sql", &claims.sub)
				.fetch_one(&**db)
				.await
			{
				Ok(user_data) => {
					if UserFlags::from(user_data.flags) & UserFlags::BANNED == UserFlags::BANNED {
						Err(AuthenticationError::Banned)
					} else {
						Ok(claims)
					}
				},
				Err(why) => {
					tracing::warn!(?why, "failed to fetch user data from db");
					Err(AuthenticationError::Other)
				},
			}
		},
		Err(why) => Err(why),
	}
}

#[derive(Debug)]
pub struct AuthUser {
	pub claims: Claims,
}

#[rocket::async_trait]
impl<'a, 'r> FromRequest<'a, 'r> for AuthUser {
	type Error = AuthenticationError;

	async fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
		let token = match request.cookies().get("token") {
			Some(token) => token,
			None => return Outcome::Forward(()),
		};

		let key = match request.managed_state::<KEY>() {
			Some(key) => key,
			None => {
				return Outcome::Failure((Status::InternalServerError, AuthenticationError::Other))
			},
		};

		let db = match request.managed_state::<Database>() {
			Some(db) => db,
			None => {
				return Outcome::Failure((Status::InternalServerError, AuthenticationError::Other))
			},
		};

		let config = match request.managed_state::<Config>() {
			Some(db) => db,
			None => {
				return Outcome::Failure((Status::InternalServerError, AuthenticationError::Other))
			},
		};

		match validate(db, token.value(), key, UserFlags::USER).await {
			Ok(claims) => Outcome::Success(AuthUser { claims }),
			Err(AuthenticationError::Banned) => {
				tracing::warn!("user banned");
				request.cookies().remove(utils::make_cookie(
					API_AUTH_TOKEN_COOKIE,
					"",
					Duration::seconds(1),
					&config.twitch.base_url,
				));
				Outcome::Forward(())
			},
			Err(why) => {
				tracing::warn!("login failure [user]: {:?}", why);
				Outcome::Forward(())
			},
		}
	}
}

#[derive(Debug)]
pub struct AuthAdmin {
	pub claims: Claims,
}

#[rocket::async_trait]
impl<'a, 'r> FromRequest<'a, 'r> for AuthAdmin {
	type Error = AuthenticationError;

	async fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
		let token = match request.cookies().get("token") {
			Some(token) => token,
			None => return Outcome::Forward(()),
		};

		let key = match request.managed_state::<KEY>() {
			Some(key) => key,
			None => {
				return Outcome::Failure((Status::InternalServerError, AuthenticationError::Other))
			},
		};

		let db = match request.managed_state::<Database>() {
			Some(db) => db,
			None => {
				return Outcome::Failure((Status::InternalServerError, AuthenticationError::Other))
			},
		};

		let config = match request.managed_state::<Config>() {
			Some(db) => db,
			None => {
				return Outcome::Failure((Status::InternalServerError, AuthenticationError::Other))
			},
		};

		match validate(db, token.value(), key, UserFlags::USER | UserFlags::ADMIN).await {
			Ok(claims) => Outcome::Success(AuthAdmin { claims }),
			Err(AuthenticationError::Banned) => {
				tracing::warn!("user banned");
				request.cookies().remove(utils::make_cookie(
					API_AUTH_TOKEN_COOKIE,
					"",
					Duration::seconds(1),
					&config.twitch.base_url,
				));
				Outcome::Forward(())
			},
			Err(why) => {
				tracing::warn!("login failure [admin]: {:?}", why);
				Outcome::Forward(())
			},
		}
	}
}

#[derive(Debug)]
pub enum AuthenticationError {
	MissingRole,
	Expired,
	IssuedInFuture,
	InvalidSignature,
	InvalidIssuer,
	InvalidAlgorithm,
	InvalidFormat,
	Banned,
	Other,
}
