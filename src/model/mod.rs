// BSD 3-Clause License
//
// Copyright (c) 2020, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

pub mod jwt_auth;
pub mod query_types;
pub mod seed;
pub mod twitch;

mod filter_item;
mod user;

pub use filter_item::Filter;
pub use query_types::*;
pub use user::*;

use crate::{constants, SERVER_NAME};
use bit_vec::BitVec;
use serde::{Deserialize, Serialize};
use std::ops::Deref;

#[derive(Debug, Clone)]
pub struct KEY([u8; crate::utils::ENC_KEY_LEN]);

impl KEY {
	pub fn new(k: [u8; crate::utils::ENC_KEY_LEN]) -> Self { Self(k) }
}

impl Deref for KEY {
	type Target = [u8];

	fn deref(&self) -> &Self::Target { &self.0 }
}

#[rustfmt::skip]
bitflags::bitflags! {
	#[derive(Serialize, Deserialize)]
	pub struct UserFlags: u16 {
		// User has user privileges
		const USER   = 0b0000_0000_0000_0001;
		// User is administrator
		const ADMIN  = 0b0000_0001_0000_0000;
		// User is owner
		const OWNER  = 0b1000_0000_0000_0000;
		// User is banned
		const BANNED = 0b0000_0000_0001_0000;
	}
}

impl Default for UserFlags {
	fn default() -> Self { Self::USER }
}

impl From<i16> for UserFlags {
	fn from(v: i16) -> Self { Self::from_bits_truncate(v as u16) }
}

impl Into<i16> for UserFlags {
	fn into(self) -> i16 { self.bits as i16 }
}

impl From<BitVec> for UserFlags {
	fn from(vc: BitVec<u32>) -> Self {
		let mut buf = [0u8; 2];
		let bt = vc.to_bytes();
		buf[1] = bt[1];
		buf[0] = bt[0];
		UserFlags::from(i16::from_be_bytes(buf))
	}
}

#[derive(Debug, serde::Serialize)]
pub struct StaticVersionInfo {
	version: &'static str,
	rev:     &'static str,
	debug:   bool,
}

impl StaticVersionInfo {
	pub fn get() -> &'static StaticVersionInfo {
		static VALUE: StaticVersionInfo = StaticVersionInfo {
			version: SERVER_NAME,
			rev:     constants::GIT_REV,
			debug:   constants::DEBUG_MODE,
		};
		&VALUE
	}
}
