// BSD 3-Clause License
//
// Copyright (c) 2020, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use crate::model::Row;
use std::iter;

pub struct Filter<'s> {
	predicate: Vec<FilterItem<'s>>,
}

impl<'s> Filter<'s> {
	pub fn parse(data: &'s str) -> Filter<'s> {
		Filter {
			predicate: FilterItem::parse_items(data).collect(),
		}
	}

	/// Match the given row against the filter list.
	/// if *any* filter matches the test passes.
	pub fn matches(&self, row: &Row) -> bool {
		for pred in &self.predicate {
			let res = match pred {
				FilterItem::Tag(tag) => row.tags.iter().any(|row_tag| row_tag.contains(tag)),
				FilterItem::PrefixTag(prefix, tag) => row
					.tags
					.iter()
					.any(|row_tag| check_row_tag(row_tag, prefix, tag)),
				FilterItem::Name(name) => row.seed.contains(name),
				FilterItem::Author(author) => row.user_name.contains(author),
				FilterItem::Any(item) => {
					row.seed.contains(item)
						|| row.user_name.contains(item)
						|| row.tags.iter().any(|row_tag| row_tag.contains(item))
				},
				FilterItem::ComplexAny(pre, name) => {
					contains_in_sequence(&row.seed, pre, name)
						|| contains_in_sequence(&row.user_name, pre, name)
						|| row
							.tags
							.iter()
							.any(|row_tag| check_row_tag(row_tag, pre, name))
				},
			};

			if !res {
				return false;
			}
		}

		true
	}
}

//#region Filter match utilities

/// Find a item, and if it exist find another item after it
#[inline]
fn contains_in_sequence(v: &str, l: &str, r: &str) -> bool {
	match v.find(l) {
		Some(idx) => v[idx..].find(r).is_some(),
		None => false,
	}
}

/// Check a row
#[inline]
fn check_row_tag(rt: &str, prefix: &str, item: &str) -> bool {
	let mut pt = rt.split(':');
	match (pt.next(), pt.next()) {
		(Some(l), Some(r)) => l.contains(prefix) && r.contains(item),
		(_, _) => false,
	}
}

//#endregion

pub enum FilterItem<'s> {
	Tag(&'s str),
	PrefixTag(&'s str, &'s str),
	Name(&'s str),
	Author(&'s str),
	Any(&'s str),
	ComplexAny(&'s str, &'s str),
}

impl<'s> FilterItem<'s> {
	pub fn parse_items(data: &'s str) -> impl Iterator<Item = FilterItem<'s>> {
		let mut parts = data.split(' ');
		iter::from_fn(move || {
			let part = parts.next()?;

			let mut data = part.splitn(2, ':');
			let key = data.next().unwrap();
			let value = data.next();

			let next = match (key, value) {
				("tag", Some(tag)) if tag.contains(':') => {
					let mut prefix_tag = tag.splitn(2, ':');
					let left = prefix_tag.next().unwrap();
					let right = prefix_tag.next().unwrap();
					FilterItem::PrefixTag(left, right)
				},
				("tag", Some(tag)) => FilterItem::Tag(tag),
				("author", Some(author)) => FilterItem::Author(author),
				("name", Some(name)) => FilterItem::Name(name),
				(value, None) => FilterItem::Any(value),
				(value, Some(other)) => FilterItem::ComplexAny(value, other),
			};

			Some(next)
		})
	}
}
