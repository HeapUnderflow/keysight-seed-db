// BSD 3-Clause License
//
// Copyright (c) 2020, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#![allow(unused_macros)]
#![allow(dead_code)]

use crate::SECURE_COOKIE;
use rand::{thread_rng, Rng};
use reqwest::{header, Url};
use rocket::http::{Cookie, CookieJar, SameSite};
use std::{borrow::Cow, error::Error, fmt, io, iter, ops::Range, time as stdtime};
use time::Duration;
use tokio::{fs, io::AsyncReadExt};

//#region Macros
/// BTreeMap literal macro
macro_rules! btmap {
    ( $($key:expr => $value:expr,)+ ) => (btmap!($($key => $value),+));

    ( $($key:expr => $value:expr),* ) => {
        {
            let mut _map = ::std::collections::BTreeMap::new();
            $(
                let _ = _map.insert($key, $value);
            )*
            _map
        }
    };
}

macro_rules! err_log {
	($e:expr) => {
		if let Err(why) = $e {
			tracing::error!("[{}#{}:{}] error = {:?}", file!(), line!(), column!(), why);
		}
	};
}
//#endregion

//#region Secret Key Loading / Formatting
/// Key length in bytes
pub const ENC_KEY_LEN: usize = 256 / 8;

/// Load a secret key from the given path.
/// If the data at path is longer then the key len required, data
/// is read and then xored onto the data in the buffer
pub async fn load_key() -> io::Result<[u8; ENC_KEY_LEN]> {
	let mut out_buf = [0u8; ENC_KEY_LEN];
	let mut tmp_buf = [0u8; ENC_KEY_LEN];

	let mut file = fs::File::open("secret.key").await?;
	if file.metadata().await?.len() < out_buf.len() as u64 {
		panic!("secret key file is less than {} bytes!", out_buf.len());
	}

	loop {
		let br = file.read(&mut tmp_buf).await?;
		if br == 0 {
			break;
		}
		for idx in 0..br {
			out_buf[idx] ^= tmp_buf[idx];
		}
	}

	Ok(out_buf)
}

/// Convert the given key to hex
pub fn format_key(key: [u8; ENC_KEY_LEN]) -> String {
	const CHARS: [u8; 16] = [
		b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', b'A', b'B', b'C', b'D', b'E',
		b'F',
	];

	// allocate 2x of KEYLEN as we need 2 characters per byte (1 per nibble)
	let mut out = String::with_capacity(ENC_KEY_LEN * 2);

	for b in key.iter() {
		out.push(CHARS[(*b >> 4) as usize] as char);
		out.push(CHARS[(*b & 0xF) as usize] as char);
	}

	out
}
//#endregion

//#region Other Utils
pub fn reqwest_client() -> reqwest::Client {
	let mut def_hdr = header::HeaderMap::new();
	def_hdr.insert(header::DNT, 1.into());
	def_hdr.insert(
		header::ACCEPT_CHARSET,
		header::HeaderValue::from_static("utf-8"),
	);

	reqwest::Client::builder()
		.gzip(true)
		.user_agent(concat!(
			env!("CARGO_PKG_NAME"),
			"/",
			env!("CARGO_PKG_VERSION"),
			" (app)"
		))
		.default_headers(def_hdr)
		.timeout(stdtime::Duration::from_millis(3000))
		.build()
		.unwrap()
}

pub type UtilsResult<T> = Result<T, UtilsError>;

#[derive(Debug, thiserror::Error)]
pub enum UtilsError {
	#[error("io error: {source} ({:?})", source.source())]
	Io {
		#[from]
		source: io::Error,
	},
	#[error("json error: {source}")]
	Json {
		#[from]
		source: serde_json::Error,
	},
}

/// Generate a alphanumeric string with length len
pub fn random_alpha(len: usize) -> String {
	const RANGES: [Range<u8>; 3] = [b'a'..b'z', b'A'..b'Z', b'0'..b'9'];
	if len == 0 {
		return String::new();
	}
	let mut rng = thread_rng();
	iter::from_fn(|| Some(rng.gen::<u8>()))
		.filter(|byte| RANGES.iter().any(|range| range.contains(byte)))
		.take(len)
		.map(|c| c as char)
		.collect::<String>()
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum MessageType {
	Success,
	Warn,
	Error,
}

impl fmt::Display for MessageType {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			MessageType::Success => write!(f, "success"),
			MessageType::Warn => write!(f, "warn"),
			MessageType::Error => write!(f, "error"),
		}
	}
}

/// Find a cookie and delete it if it exists, returning its value
pub fn get_message_cookie<'c>(jar: &'c CookieJar<'c>, typ: MessageType) -> Option<String> {
	let cookie = jar.get_private(&format!("{}_{}", typ, "message"));

	match cookie {
		Some(data) => {
			jar.remove_private(Cookie::named(format!("{}_{}", typ, "message")));
			Some(data.value().to_owned())
		},
		None => None,
	}
}

#[tracing::instrument(skip(jar))]
pub fn set_message_cookie(
	jar: &CookieJar<'_>,
	typ: MessageType,
	mut message: String,
	escape: bool,
) {
	if escape {
		message = ammonia::clean(&message);
	}

	let mut cookie = Cookie::new(format!("{}_{}", typ, "message"), message);
	cookie.set_max_age(time::Duration::minutes(10));
	cookie.set_http_only(true);
	cookie.set_secure(SECURE_COOKIE);
	cookie.set_same_site(SameSite::Lax);

	jar.add_private(cookie);

	tracing::trace!("wrote message cookie");
}

/// Create a cookie with parsed path
pub fn make_cookie<'c, N, V>(name: N, value: V, expires_in: Duration, base_url: &str) -> Cookie<'c>
where
	N: Into<Cow<'c, str>>,
	V: Into<Cow<'c, str>>,
{
	let mut cookie = Cookie::new(name, value);
	cookie.set_max_age(expires_in);
	cookie.set_http_only(true);
	cookie.set_secure(SECURE_COOKIE);
	cookie.set_same_site(SameSite::Lax);
	let uri_url = Url::parse(base_url);

	cookie.set_path(
		uri_url
			.as_ref()
			.map(|v| v.path().to_owned())
			.unwrap_or_else(|_| "/".to_owned()),
	);

	cookie
}

#[derive(Debug, serde::Serialize)]
pub struct MessageBlock {
	pub success: Option<String>,
	pub warn:    Option<String>,
	pub error:   Option<String>,
}

impl MessageBlock {
	pub fn from_jar(jar: &CookieJar<'_>) -> MessageBlock {
		let success = get_message_cookie(jar, MessageType::Success);
		let warn = get_message_cookie(jar, MessageType::Warn);
		let error = get_message_cookie(jar, MessageType::Error);

		MessageBlock {
			success,
			warn,
			error,
		}
	}
}

#[rustfmt::skip]
pub fn clamp2<T: PartialOrd>(v: T, l: T, h: T) -> T {
	if v < l { return l }
	if v > h { return h }
	v
}

/// Convert a T to a Option<T>
pub trait ContentOrNone: Sized {
	fn content_or_none(self) -> Option<Self>;
}

macro_rules! _impl_content_or_none_str {
	($($t:ty $(, $lt:lifetime)?);*) => {
	$(
		impl$(<$lt>)? ContentOrNone for $(&$lt )? $t {
			fn content_or_none(self) -> Option<$(&$lt )? $t> {
				if self.is_empty() { None } else { Some(self) }
			}
		}
	)*
	}
}

_impl_content_or_none_str!(String; String, 'a; str, 'a);
//#endregion
