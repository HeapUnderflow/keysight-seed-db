// BSD 3-Clause License
//
// Copyright (c) 2020, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use crate::SERVER_NAME;
use rocket::{
	fairing::{Fairing, Info, Kind},
	http::{uri::Origin, Header},
	Data,
	Request,
	Response,
};

/// Fairing to override the server header to "$CARGO_PKG_NAME/$CARGO_PKG_VERSION"
pub struct ServerHeader;

#[rocket::async_trait]
impl Fairing for ServerHeader {
	fn info(&self) -> Info {
		Info {
			name: "Force Server Header",
			kind: Kind::Response,
		}
	}

	async fn on_response<'r>(&self, _: &'r Request<'_>, res: &mut Response<'r>) {
		res.set_header(Header::new("server", SERVER_NAME));
	}
}

pub struct LogFairing;

#[rocket::async_trait]
impl Fairing for LogFairing {
	fn info(&self) -> Info {
		Info {
			name: "Log Fairing",
			kind: Kind::Launch | Kind::Request | Kind::Response,
		}
	}

	// async fn on_launch(&self, _: &Rocket) {
	// 	tracing::info!("rocket launched...");
	// }

	async fn on_request(&self, req: &mut Request<'_>, _: &mut Data) {
		tracing::trace!(
			method=%req.method(),
			remote=%req.client_ip().map(|sock_addr| format!("{}", sock_addr)).unwrap_or_else(|| String::from("-")),
			path=%fmt_uri(&req.uri()),
			"inbound"
		);
	}

	async fn on_response<'r>(&self, req: &'r Request<'_>, res: &mut Response<'r>) {
		tracing::info!(
			method=%req.method(),
			status=?res.status().to_string(),
			remote=%req.client_ip().map(|sock_addr| format!("{}", sock_addr)).unwrap_or_else(|| String::from("-")),
			path=%fmt_uri(&req.uri()),
			content_type=?res.content_type().as_ref().map(|v| format!("{}/{}", v.top(), v.sub())),
			"handled"
		);
	}
}

fn fmt_uri(uri: &Origin<'_>) -> String {
	let mut out = String::new();

	out.push_str(uri.path());

	if let Some(query) = uri.query() {
		out.push('?');
		out.push_str(&query);
	}

	out
}
