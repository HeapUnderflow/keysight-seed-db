// BSD 3-Clause License
//
// Copyright (c) 2020, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use crate::{constants::GIT_REV, SERVER_NAME};
use rocket::{http::Status, response::status::Custom, Request};
use rocket_contrib::templates::Template;

#[allow(non_snake_case)]
pub fn CATCHERS() -> Vec<rocket::Catcher> {
	let mut out = Vec::new();
	// Auto generated catchers from macro
	out.extend(generated_catchers());
	// Hand written catchers here
	out.extend(rocket::catchers![]);
	out
}

#[derive(Debug, serde::Serialize)]
struct ErrorPage<'p> {
	code:        u16,
	reason:      &'p str,
	description: Option<&'p str>,
	server:      &'p str,
	rev:         &'p str,
}

macro_rules! error_page {
	($(catcher $fn:ident => $code:expr, $name:expr, $reason:expr;)+) => {
		fn generated_catchers() -> Vec<::rocket::Catcher> {
			::rocket::catchers![
				$($fn),+
			]
		}

		$(
			#[rocket::catch($code)]
			pub async fn $fn(status: Status, _: &Request<'_>) -> Custom<Template> {
				Custom(
					status,
					Template::render(
						"error/error",
						ErrorPage {
							code: $code,
							reason: $name,
							description: Some($reason),
							server: SERVER_NAME,
							rev: GIT_REV
						}
					)
				)
			}
		)*
	}
}

error_page! {
	catcher bad_request => 400, "Bad Request", "The request could not be understood by the server due \
		to malformed syntax.";
	catcher unauthorized => 401, "Unauthorized", "The request requires user authentication.";
	catcher payment_required => 402, "Payment Required", "The request could not be processed due to lack of payment.";
	catcher forbidden => 403, "Forbidden", "The server refused to authorize the request.";
	catcher not_found => 404, "Not Found", "The requested resource could not be found.";
	catcher method_not_allowed => 405, "Method Not Allowed", "The request method is not supported for the requested resource.";
	catcher not_acceptable => 406, "Not Acceptable", "The requested resource is capable of generating only content not \
		acceptable according to the Accept headers sent in the request.";
	catcher proxy_authentication_required => 407, "Proxy Authentication Required", "Authentication with the proxy is required.";
	catcher request_timeout => 408, "Request Timeout", "The server timed out waiting for the request.";
	catcher conflict => 409, "Conflict", "The request could not be processed because of a conflict in the request.";
	catcher gone => 410, "Gone", "The resource requested is no longer available and will not be available again.";
	catcher length_required => 411, "Length Required", "The request did not specify the length of its content, which is \
		required by the requested resource.";
	catcher precondition_failed => 412, "Precondition Failed", "The server does not meet one of the \
		preconditions specified in the request.";
	catcher payload_too_large => 413, "Payload Too Large", "The request is larger than the server is \
		willing or able to process.";
	catcher uri_too_long => 414, "URI Too Long", "The URI provided was too long for the server to process.";
	catcher unsupported_media_type => 415, "Unsupported Media Type", "The request entity has a media type which \
		the server or resource does not support.";
	catcher range_not_satisfiable => 416, "Range Not Satisfiable", "The portion of the requested file cannot be \
		supplied by the server.";
	catcher expectation_failed => 417, "Expectation Failed", "The server cannot meet the requirements of the \
		Expect request-header field.";
	catcher im_a_teapot => 418, "I'm a teapot", "I was requested to brew coffee, and I am a teapot.";
	catcher misdirected_request => 421, "Misdirected Request", "The server cannot produce a response for this request.";
	catcher unprocessable_entity => 422, "Unprocessable Entity", "The request was well-formed but was unable to \
		be followed due to semantic errors.";
	catcher update_required => 426, "Upgrade Required", "Switching to the protocol in the Upgrade header field is required.";
	catcher precondition_required => 428, "Precondition Required", "The server requires the request to be conditional.";
	catcher too_many_requests => 429, "Too Many Requests", "Too many requests have been received recently.";
	catcher request_header_fields_too_large => 431, "Request Header Fields Too Large", "The server is unwilling to process \
		the request because either an individual header field, or all the header \
		fields collectively, are too large.";
	catcher unavaliable_for_legal_reasons => 451, "Unavailable For Legal Reasons", "The requested resource is unavailable \
		due to a legal demand to deny access to this resource.";
	catcher internal_server_error => 500, "Internal Server Error", "The server encountered an internal error while \
		processing this request.";
	catcher not_implemented => 501, "Not Implemented", "The server either does not recognize the request \
		method, or it lacks the ability to fulfill the request.";
	catcher service_unavaliable => 503, "Service Unavailable", "The server is currently unavailable.";
	catcher gateway_timeout => 504, "Gateway Timeout", "The server did not receive a timely response from an upstream server.";
	catcher not_extended => 510, "Not Extended", "Further extensions to the request are required for \
		the server to fulfill it.";
}
