// BSD 3-Clause License
//
// Copyright (c) 2020, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use crate::{
	config::Mode,
	db::Database,
	fairings::{LogFairing, ServerHeader},
	model::KEY,
	utils::reqwest_client,
};
use rocket::{
	figment::{providers::Env, Figment},
	Config,
};
use rocket_contrib::{
	crate_relative,
	helmet::SpaceHelmet,
	serve::StaticFiles,
	templates::Template,
};
use std::env::VarError;

#[macro_use]
mod utils;
mod catchers;
mod config;
mod constants;
mod db;
mod fairings;
mod guards;
mod model;
mod page;

#[cfg(debug_assertions)]
pub const SERVER_NAME: &str = concat!(
	env!("CARGO_PKG_NAME"),
	"/v",
	env!("CARGO_PKG_VERSION"),
	"-debug"
);
#[cfg(not(debug_assertions))]
pub const SERVER_NAME: &str = concat!(env!("CARGO_PKG_NAME"), "/v", env!("CARGO_PKG_VERSION"));

#[cfg(debug_assertions)]
pub const SECURE_COOKIE: bool = false;
#[cfg(not(debug_assertions))]
pub const SECURE_COOKIE: bool = true;

/// [`try`] like macro to escape early from main
macro_rules! se_checked {
	($e:expr) => {{
		match $e {
			Ok(val) => val,
			Err(why) => {
				tracing::error!(line=%line!(), error=?why, "encountered error while starting up");
				return Err(());
			}
		}
	}}
}

async fn realmain() -> Result<(), ()> {
	tracing::info!(rev=?constants::GIT_REV, target=?constants::TARGET, profile=?constants::PROFILE, "starting keysight db server");

	if let Err(why) = dotenv::dotenv() {
		let r = match why {
			dotenv::Error::Io(io) => match io.kind() {
				std::io::ErrorKind::NotFound => Ok(()),
				_ => Err(dotenv::Error::Io(io)),
			},
			other => Err(other),
		};

		se_checked!(r);
	}

	let key = se_checked!(utils::load_key().await);

	let service_config = se_checked!(config::Config::load("config.json").await);

	let default_config = match service_config.service.mode {
		Mode::Dev => Config::debug_default(),
		Mode::Prod => Config::release_default(),
	};

	let config_figment = Figment::from(default_config)
		.merge(("port", service_config.service.port))
		.merge(("secret_key", key))
		.merge(("template_dir", "web/"))
		.merge(("log_level", "off"))
		.merge(Env::prefixed("KEYSIGHT_DB_"));

	let database = se_checked!(
		Database::new(&se_checked!(std::env::var("DATABASE_URL").map_err(
			|e| match e {
				VarError::NotPresent => String::from("missing DATABASE_URL in environment"),
				e => format!("{:?}", e),
			}
		)))
		.await
	);

	let result = rocket::custom(config_figment)
		// XSS Protection & More
		.attach(SpaceHelmet::default())
		// Templating support
		.attach(Template::fairing())
		// Server header fairing
		.attach(ServerHeader)
		// Log Fairing
		.attach(LogFairing)
		// Service configuration
		.manage(service_config)
		// Database pool connection
		.manage(database)
		// Http client for external requests
		.manage(reqwest_client())
		// Secret key used for encryption and signing
		.manage(KEY::new(key))
		// Custom error catchers
		.register(catchers::CATCHERS())
		// Primary routes
		.mount("/", page::MAIN_ROUTES())
		// Routes for api access, login & more
		.mount("/api", page::API_ROUTES())
		// Special routes that do not fit in base
		.mount("/special", page::SPECIAL_ROUTES())
		// Static files from "$CRATE_ROOT/web/static"
		.mount("/static", StaticFiles::from(crate_relative!("web/static")))
		.launch()
		.await;

	match result {
		Ok(()) => Ok(()),
		Err(why) => {
			tracing::error!(?why, "FATAL SERVER ERROR");
			Err(())
		},
	}
}

fn main() {
	tracing_subscriber::fmt::init();

	// copy pasted from tokio's `main` macro
	let result = tokio::runtime::Builder::new_multi_thread()
		.enable_all()
		.build()
		.unwrap()
		.block_on(realmain());

	match result {
		Ok(()) => (),
		Err(()) => std::process::exit(1),
	}
}
