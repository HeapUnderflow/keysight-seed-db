// BSD 3-Clause License
//
// Copyright (c) 2021, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use crate::{
	db::Database,
	model::{
		jwt_auth::{AuthAdmin, AuthUser},
		StaticVersionInfo,
	},
	page::api::RelRedirectTo,
	utils,
};
use rocket::{
	http::{CookieJar, Status},
	State,
};
use rocket_contrib::templates::Template;
use serde::Serialize;

#[allow(non_snake_case)]
pub fn ROUTES() -> Vec<rocket::Route> { rocket::routes![edit_admin, edit_user, edit_anon] }

#[rocket::get("/edit?<seed>", rank = 1)]
pub async fn edit_admin(
	jar: &CookieJar<'_>,
	db: State<'_, Database>,
	_admin: AuthAdmin,
	seed: String,
) -> Result<Template, RelRedirectTo<'static>> {
	Ok(Template::render(
		"edit",
		load_data(jar, &*db, seed)
			.await
			.ok_or_else(|| RelRedirectTo::new("../"))?,
	))
}

#[rocket::get("/edit?<seed>", rank = 2)]
pub async fn edit_user(
	jar: &CookieJar<'_>,
	db: State<'_, Database>,
	_user: AuthUser,
	seed: String,
) -> Result<Template, RelRedirectTo<'static>> {
	Ok(Template::render(
		"edit",
		load_data(jar, &*db, seed)
			.await
			.ok_or_else(|| RelRedirectTo::new("../"))?,
	))
}

#[rocket::get("/edit", rank = 3)]
pub async fn edit_anon() -> Status { Status::Unauthorized }

async fn load_data(jar: &CookieJar<'_>, db: &Database, seed: String) -> Option<SiteData> {
	let message = utils::MessageBlock::from_jar(jar);

	let seed = match sqlx::query_file!("queries/get_seed.sql", seed)
		.fetch_one(&**db)
		.await
	{
		Ok(seed) => Seed {
			name:  seed.id,
			image: seed.image,
			tags:  seed.tags.join(","),
		},
		Err(why) => {
			tracing::info!(?why, "could not load seed");
			return None;
		},
	};

	Some(SiteData {
		seed,
		message,
		version_info: StaticVersionInfo::get(),
	})
}

#[derive(Debug, Serialize)]
struct SiteData {
	seed:         Seed,
	message:      utils::MessageBlock,
	version_info: &'static StaticVersionInfo,
}

#[derive(Debug, Serialize)]
struct Seed {
	name:  String,
	image: Option<String>,
	tags:  String,
}
