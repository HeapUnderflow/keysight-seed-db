// BSD 3-Clause License
//
// Copyright (c) 2021, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use crate::{
	db::Database,
	model::{
		jwt_auth::{AuthAdmin, AuthUser, Claims},
		seed::Tag,
		Filter,
		Row,
		StaticVersionInfo,
	},
	utils,
	utils::clamp2,
};
use bit_vec::BitVec;
use futures::{StreamExt, TryStreamExt};
use rocket::{http::CookieJar, State};
use rocket_contrib::templates::Template;
use serde::Serialize;

const PAGE_SIZE: i64 = 25;

#[allow(non_snake_case)]
pub fn ROUTES() -> Vec<rocket::Route> { rocket::routes![index_admin, index_user, index_anon] }

#[derive(Debug, Serialize)]
struct SiteData {
	user:         Option<UserData>,
	seeds:        Vec<Seed>,
	message:      utils::MessageBlock,
	total_seeds:  i64,
	page:         Pagination,
	search:       Option<String>,
	version_info: &'static StaticVersionInfo,
}

#[derive(Debug, Serialize)]
struct Pagination {
	current: i64,
	min:     i64,
	max:     i64,
	before:  Option<i64>,
	next:    Option<i64>,
}

#[derive(Debug, Serialize)]
struct UserData {
	name:  String,
	image: Option<String>,
	admin: bool,
}

#[derive(Debug, Serialize)]
struct Seed {
	name:        String,
	author:      String,
	show_delete: bool,
	show_edit:   bool,
	image:       Option<String>,
	tags:        Vec<Tag>,
	created:     i64,
	updated:     i64,
}

fn db_fetch_err(o: &mut utils::MessageBlock) {
	// Using mem::take here to disassociate o.error from o, which allows us to then do a .or_else default followed by a map
	o.error = std::mem::take(&mut o.error)
		.or_else(|| Some(String::new()))
		.map(|mut value| {
			value.push('\n');
			value.push_str("failed to fetch seeds from database");
			value
		});
}

#[allow(clippy::too_many_arguments)]
#[tracing::instrument(skip(db, claims, msg_perm_predicate))]
async fn load_data(
	db: &Database,
	claims: Option<Claims>,
	admin: bool,
	message: utils::MessageBlock,
	mut search: Option<String>,
	page: i64,
	page_size: i64,
	msg_perm_predicate: Box<dyn Fn(&Row, Option<&Claims>) -> bool + Send + Sync>,
) -> SiteData {
	assert!(page >= 0, "page is less than 0");
	assert!(page_size >= 1, "page size is less than 1");

	if search.as_ref().map(|v| v.is_empty()).unwrap_or(false) {
		search = None;
	}

	let mut site_data = SiteData {
		user: claims.as_ref().map(|user_data| UserData {
			name: user_data.kssdb_twitch.name.to_owned(),
			image: user_data.kssdb_twitch.img.to_owned(),
			admin,
		}),
		seeds: vec![],
		message,
		page: Pagination {
			current: page,
			min:     0,
			max:     0,
			before:  None,
			next:    None,
		},
		total_seeds: 0,
		search: search.clone(),
		version_info: StaticVersionInfo::get(),
	};

	match sqlx::query_file!("queries/count_seeds.sql")
		.fetch_one(&**db)
		.await
	{
		Ok(count) => {
			let count = count.count;
			let max_page = (count as f64 / PAGE_SIZE as f64).floor() as i64;
			site_data.page = Pagination {
				current: page,
				min:     0,
				max:     max_page,
				before:  if page == 0 { None } else { Some(page - 1) },
				next:    if page == max_page {
					None
				} else {
					Some(page + 1)
				},
			};

			site_data.total_seeds = count;
		},
		Err(why) => {
			tracing::warn!(?why, "failed to retrieve seeds from db");
			db_fetch_err(&mut site_data.message);
			site_data.seeds = Vec::default();
			return site_data;
		},
	}

	// Creating a dynamic fn. Avoids having to re-parse Filter on every row
	let filter_fn: Box<dyn Fn(&Row) -> bool + Send + Sync + '_> =
		match search.as_ref().map(|term| Filter::parse(&term)) {
			Some(filter) => Box::new(move |search: &Row| filter.matches(search)),
			None => Box::new(|_: &Row| true),
		};

	#[allow(clippy::toplevel_ref_arg)]
	let seeds_result = sqlx::query_file!("queries/get_seeds.sql")
		.fetch(&**db)
		.try_filter_map(|row| async {
			let row = Row {
				seed:       row.seed.trim().to_owned(),
				user_name:  row.user_name.trim().to_owned(),
				user_id:    row.user_id.trim().to_owned(),
				user_image: row.user_image,
				seed_image: row.seed_image,
				tags:       row.tags,
				created:    row.created,
				updated:    row.updated,
				flags:      row.flags.into(),
			};

			if filter_fn(&row) {
				Ok(Some(row))
			} else {
				Ok(None)
			}
		})
		.skip((page_size * page) as usize)
		.take(page_size as usize)
		.map_ok(|row| {
			let show_delete: bool = msg_perm_predicate(&row, claims.as_ref());
			Seed {
				name: row.seed,
				author: row.user_name,
				show_delete,
				show_edit: show_delete,
				image: row.seed_image,
				tags: row.tags.iter().map(|el| Tag::new(el)).collect(),
				created: row.created.unix_timestamp(),
				updated: row.updated.unix_timestamp(),
			}
		})
		.try_collect::<Vec<_>>()
		.await;

	match seeds_result {
		Ok(seeds) => site_data.seeds = seeds,
		Err(why) => {
			tracing::warn!(?why, "failed to retrieve seeds from db");
			db_fetch_err(&mut site_data.message);
			site_data.seeds = Vec::default();
		},
	}

	site_data
}

#[rocket::get("/?<page>&<filter>", rank = 1)]
pub async fn index_admin(
	jar: &CookieJar<'_>,
	db: State<'_, Database>,
	_admin: AuthAdmin,
	page: Option<i64>,
	filter: Option<String>,
) -> Template {
	let message = utils::MessageBlock::from_jar(jar);

	let page = page
		.map(|v| clamp2(v, 0, i32::max_value() as i64))
		.unwrap_or(0);
	let site_data = load_data(
		&*db,
		Some(_admin.claims),
		true,
		message,
		filter,
		page,
		PAGE_SIZE,
		Box::new(move |_, _| true),
	)
	.await;

	Template::render("index", site_data)
}

#[rocket::get("/?<page>&<filter>", rank = 2)]
pub async fn index_user(
	jar: &CookieJar<'_>,
	db: State<'_, Database>,
	_user: AuthUser,
	page: Option<i64>,
	filter: Option<String>,
) -> Template {
	let message = utils::MessageBlock::from_jar(jar);

	let page = page
		.map(|v| clamp2(v, 0, i32::max_value() as i64))
		.unwrap_or(0);
	let site_data = load_data(
		&*db,
		Some(_user.claims),
		false,
		message,
		filter,
		page,
		PAGE_SIZE,
		Box::new(move |row, claims| {
			claims
				.map(|cl| cl.sub == row.user_id.trim())
				.unwrap_or(false)
		}),
	)
	.await;

	Template::render("index", site_data)
}

#[rocket::get("/?<page>&<filter>", rank = 3)]
pub async fn index_anon(
	jar: &CookieJar<'_>,
	db: State<'_, Database>,
	page: Option<i64>,
	filter: Option<String>,
) -> Template {
	let message = utils::MessageBlock::from_jar(jar);

	let page = page
		.map(|v| clamp2(v, 0, i32::max_value() as i64))
		.unwrap_or(0);
	let site_data = load_data(
		&*db,
		None,
		false,
		message,
		filter,
		page,
		PAGE_SIZE,
		Box::new(move |_, _| false),
	)
	.await;

	Template::render("index", site_data)
}
