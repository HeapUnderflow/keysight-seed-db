// BSD 3-Clause License
//
// Copyright (c) 2021, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use crate::{
	db::Database,
	model::{jwt_auth::AuthAdmin, StaticVersionInfo},
	page::api::RelRedirectTo,
	utils,
	utils::MessageType,
};
use rocket::{http::CookieJar, State};
use rocket_contrib::templates::Template;

#[allow(non_snake_case)]
pub fn ROUTES() -> Vec<rocket::Route> { rocket::routes![index_bans] }

#[tracing::instrument(skip(_admin, jar, db))]
#[rocket::get("/banhammer")]
pub async fn index_bans(
	_admin: AuthAdmin,
	jar: &CookieJar<'_>,
	db: State<'_, Database>,
) -> Result<Template, RelRedirectTo<'static>> {
	let banned_user_names = match sqlx::query_file!("queries/get_banned.sql")
		.fetch_all(&**db)
		.await
	{
		Ok(data) => data
			.into_iter()
			.map(|row| (row.id.trim().to_owned(), row.name.trim().to_owned()))
			.collect::<Vec<_>>(),
		Err(why) => {
			tracing::error!(?why, "failed to load banned users");
			utils::set_message_cookie(
				jar,
				MessageType::Error,
				"unable to fetch banned users (ISE: 500)".to_owned(),
				true,
			);
			return Err(RelRedirectTo::new("../"));
		},
	};

	#[derive(serde::Serialize, Debug)]
	struct BannedData {
		users:        Vec<(String, String)>,
		message:      Option<String>,
		version_info: &'static StaticVersionInfo,
	}

	let data = BannedData {
		users:        banned_user_names,
		message:      utils::get_message_cookie(jar, MessageType::Error),
		version_info: StaticVersionInfo::get(),
	};

	Ok(Template::render("banned", data))
}
