// BSD 3-Clause License
//
// Copyright (c) 2020, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

mod actions;
mod auth;

use rocket::{
	http::{Header, Status},
	response::Responder,
	Request,
	Response,
};
use std::borrow::Cow;

pub const AUTH_TOKEN_KEY: &str = "token";
pub const STATE_TOKEN_KEY: &str = "state";

#[allow(non_snake_case)]
pub fn ROUTES() -> Vec<rocket::Route> {
	let mut routes = Vec::new();

	routes.extend(auth::ROUTES());
	routes.extend(actions::ROUTES());

	routes
}

pub struct RelRedirectTo<'h> {
	data: Cow<'h, str>,
}

impl<'r, 'o: 'r, 'h: 'o> Responder<'r, 'o> for RelRedirectTo<'h> {
	fn respond_to(self, _: &'r Request<'_>) -> rocket::response::Result<'o> {
		Response::build()
			.status(Status::SeeOther)
			// here we use the Cow like a &str, and rustc will do the rest for us
			.header(Header::new("Location", self.data))
			.ok()
	}
}

impl<'h> RelRedirectTo<'h> {
	/// Create a new redirect from a borrowed str
	pub fn new(location: &'h str) -> Self {
		Self {
			data: Cow::Borrowed(location),
		}
	}
	/// Create a new redirect from a owned string
	pub fn new_owned(location: String) -> Self {
		Self {
			data: Cow::Owned(location),
		}
	}
}
