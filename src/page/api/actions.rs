// BSD 3-Clause License
//
// Copyright (c) 2020, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use rocket::http::uri::Uri;

mod ban;
mod create;
mod delete;
mod edit;

#[allow(non_snake_case)]
pub fn ROUTES() -> Vec<rocket::Route> {
	rocket::routes![
		create::user,
		create::anon,
		edit::admin,
		edit::user,
		edit::anon,
		delete::admin,
		delete::user,
		delete::anon,
		ban::ban_user,
		ban::ban_anon,
		ban::unban_user,
		ban::unban_anon,
	]
}

#[inline]
pub(self) fn sanitize_uri(uri: Option<String>) -> Result<Option<String>, ()> {
	let img = uri.and_then(|url| if url.is_empty() { None } else { Some(url) });
	if img
		.as_ref()
		.map(|el| Uri::parse(&el).is_err())
		.unwrap_or(false)
	{
		Err(())
	} else {
		Ok(img)
	}
}

#[derive(Debug, thiserror::Error)]
pub(self) enum TagSanitizeError {
	#[error("you cannot have more than 32 tags")]
	TooManyTags,
	#[error("a whole tag cannot be longer than 65 characters")]
	TagTooLong,
}

#[inline]
pub(self) fn sanitize_tags(mut tag_string: String) -> Result<Vec<String>, TagSanitizeError> {
	tag_string = ammonia::clean(&tag_string);

	let mut tag_iter = tag_string.split(',').map(|v| v.trim());

	let tags = tag_iter.by_ref().take(32).collect::<Vec<_>>();

	if tag_iter.next().is_some() {
		return Err(TagSanitizeError::TooManyTags);
	}
	if tags.iter().any(|el| el.len() > 65) {
		return Err(TagSanitizeError::TagTooLong);
	}

	let tags = tags.into_iter().map(String::from).collect::<Vec<_>>();

	Ok(tags)
}
