// BSD 3-Clause License
//
// Copyright (c) 2020, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// -- No admin route as the normal "User" creation will work just fine for this.

use crate::{
	db::Database,
	model::jwt_auth::AuthUser,
	page::api::RelRedirectTo,
	utils::MessageType,
};
use rocket::{
	http::{CookieJar, Status},
	request::Form,
	State,
};

// FIXME: Form Loading
#[tracing::instrument(skip(user, db, jar))]
#[rocket::post("/action/create", data = "<form>", rank = 1)]
pub async fn user(
	user: AuthUser,
	db: State<'_, Database>,
	jar: &CookieJar<'_>,
	form: Form<CreateFormData>,
) -> RelRedirectTo<'static> {
	let redirect = RelRedirectTo::new("../../../");

	let mut form = form.into_inner();

	form.name = ammonia::clean(&form.name);

	form.name = form.name.replace(' ', "").to_lowercase();
	let mut out_msg = String::new();
	if form.name.len() > 125 {
		out_msg.push_str("error: name cannot be longer than 125 characters<br>");
	}

	let tags = match super::sanitize_tags(form.tags) {
		Ok(v) => v,
		Err(error) => {
			out_msg.push_str(&format!("error: {}<br>", error));
			Vec::default()
		},
	};

	let image = match super::sanitize_uri(form.image) {
		Ok(image) => image,
		Err(why) => {
			out_msg.push_str("error: the image url is not a valid url<br>");
			None
		},
	};

	if !out_msg.is_empty() {
		crate::utils::set_message_cookie(jar, MessageType::Error, out_msg, false);
		return redirect;
	}

	match sqlx::query_file!(
		"queries/create_seed.sql",
		form.name,
		user.claims.sub,
		image,
		&tags[..]
	)
	.execute(&**db)
	.await
	{
		Ok(_) => redirect,
		Err(why) => {
			tracing::error!(?why);
			redirect
		},
	}
}

#[rocket::post("/action/create", rank = 2)]
pub async fn anon() -> Status { Status::Unauthorized }

#[derive(rocket::FromForm, Debug)]
pub struct CreateFormData {
	name:  String,
	tags:  String,
	image: Option<String>,
}
