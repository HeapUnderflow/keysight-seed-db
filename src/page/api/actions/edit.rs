// BSD 3-Clause License
//
// Copyright (c) 2020, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use crate::{
	db::Database,
	model::jwt_auth::{AuthAdmin, AuthUser},
	page::api::RelRedirectTo,
	utils::{ContentOrNone, MessageType},
};
use rocket::{
	http::{CookieJar, Status},
	request::Form,
	State,
};

const REDIR_PATH: &str = "../../";
const REDIR_PATH_EDIT: &str = "../../edit";
const SUCCESS_EDIT_MSG: &str = "Success";

// noinspection DuplicatedCode
#[tracing::instrument(skip(message, db, check))]
async fn update_seed<F>(message: &mut String, db: &Database, form_data: UpdateForm, check: F)
where
	F: FnOnce(&str) -> bool,
{
	let name = ammonia::clean(&form_data.id);

	let tags = match super::sanitize_tags(form_data.tags) {
		Ok(tgs) => tgs,
		Err(why) => {
			message.push_str(&format!("error: {}<br>", why));
			Vec::default()
		},
	};

	let image = match super::sanitize_uri(form_data.image) {
		Ok(uri) => uri,
		Err(()) => {
			message.push_str("error: the image url is not a valid url<br>");
			None
		},
	};

	let result = match sqlx::query_file!("queries/get_seed.sql", &name)
		.fetch_one(&**db)
		.await
	{
		Ok(query_res) => {
			let chk = check(&query_res.user_id.trim_end());
			if !chk {
				message.push_str("you dont have permission to edit this seed<br>");
			}
			chk
		},
		Err(why) => {
			tracing::debug!(?why, "query result");
			match why {
				sqlx::Error::RowNotFound => {
					message.push_str(&format!(
						"failed to update seed: seed <code>{}</code> not found<br>",
						&name
					));
					false
				},
				_ => {
					tracing::error!(?why, "failed to fetch seed");
					message.push_str(&format!("failed to delete seed {}<br>", form_data.id));
					false
				},
			}
		},
	};

	if result {
		if let Err(why) = sqlx::query_file!("queries/update_seed.sql", &name, &tags, image)
			.execute(&**db)
			.await
			.map(|v| v.rows_affected())
		{
			tracing::debug!(?why, "update result");
			match why {
				sqlx::Error::RowNotFound => (),
				_ => {
					tracing::error!(?why, "failed to update seed");
					message.push_str(&format!("failed to update seed {}<br>", form_data.id));
				},
			}
		}
	}
}

fn redir_on_msg(
	jar: &CookieJar<'_>,
	m: Option<String>,
	n: &str,
	or: &str,
) -> RelRedirectTo<'static> {
	match m {
		Some(c) => {
			crate::utils::set_message_cookie(jar, MessageType::Error, c, false);
			RelRedirectTo::new_owned(format!("{}?seed={}", REDIR_PATH_EDIT, n))
		},
		None => {
			crate::utils::set_message_cookie(jar, MessageType::Success, or.to_owned(), false);
			RelRedirectTo::new(REDIR_PATH)
		},
	}
}

#[rocket::post("/action/edit", data = "<form>", rank = 1)]
pub async fn admin(
	_admin: AuthAdmin,
	db: State<'_, Database>,
	jar: &CookieJar<'_>,
	form: Form<UpdateForm>,
) -> RelRedirectTo<'static> {
	let form = form.into_inner();
	let mut message = String::new();
	let name = form.id.clone();

	update_seed(&mut message, &*db, form, |_| true).await;
	redir_on_msg(jar, message.content_or_none(), &name, SUCCESS_EDIT_MSG)
}

#[rocket::post("/action/edit", data = "<form>", rank = 2)]
pub async fn user(
	user: AuthUser,
	db: State<'_, Database>,
	jar: &CookieJar<'_>,
	form: Form<UpdateForm>,
) -> RelRedirectTo<'static> {
	let form = form.into_inner();
	let mut message = String::new();
	let name = form.id.clone();

	update_seed(&mut message, &*db, form, |uid| user.claims.sub == uid).await;
	redir_on_msg(jar, message.content_or_none(), &name, SUCCESS_EDIT_MSG)
}

#[rocket::post("/action/edit", rank = 3)]
pub async fn anon() -> Status { Status::Unauthorized }

#[derive(rocket::FromForm, Debug)]
pub struct UpdateForm {
	id:    String,
	tags:  String,
	image: Option<String>,
}
