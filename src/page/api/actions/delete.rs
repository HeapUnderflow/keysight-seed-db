// BSD 3-Clause License
//
// Copyright (c) 2020, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use crate::{
	db::Database,
	model::jwt_auth::{AuthAdmin, AuthUser},
	page::api::RelRedirectTo,
	utils::MessageType,
};
use rocket::{
	http::{CookieJar, Status},
	State,
};

async fn delete_seed(db: &Database, jar: &CookieJar<'_>, target: &str) -> bool {
	match sqlx::query_file!("queries/delete_seed.sql", &target)
		.execute(&**db)
		.await
	{
		Ok(done) => {
			tracing::info!(count=%done.rows_affected(), ?target, "deleted row");
			true
		},
		Err(why) => {
			tracing::error!(?why, "failed to delete seed");
			crate::utils::set_message_cookie(
				jar,
				MessageType::Error,
				format!("failed to delete seed {}", target),
				true,
			);
			false
		},
	}
}

#[tracing::instrument(skip(_admin, db, jar))]
#[rocket::get("/action/delete/<target>", rank = 1)]
pub async fn admin(
	_admin: AuthAdmin,
	db: State<'_, Database>,
	jar: &CookieJar<'_>,
	target: String,
) -> RelRedirectTo<'static> {
	tracing::debug!("deleting seed");
	delete_seed(&*db, jar, &target).await;
	RelRedirectTo::new("../../../")
}

#[tracing::instrument(skip(user, db, jar))]
#[rocket::get("/action/delete/<target>", rank = 2)]
pub async fn user(
	user: AuthUser,
	db: State<'_, Database>,
	jar: &CookieJar<'_>,
	target: String,
) -> RelRedirectTo<'static> {
	let redirect = RelRedirectTo::new("../../../");

	tracing::trace!(?target);
	let result = match sqlx::query_file!("queries/get_seed.sql", &target)
		.fetch_one(&**db)
		.await
	{
		Ok(result) => result.user_id.trim() == user.claims.sub,
		Err(why) => {
			tracing::debug!(?why, "query result");
			return match why {
				sqlx::Error::RowNotFound => redirect,
				_ => {
					tracing::error!(?why, "failed to fetch seed");
					crate::utils::set_message_cookie(
						jar,
						MessageType::Error,
						format!("failed to delete seed {}", target),
						true,
					);
					redirect
				},
			};
		},
	};

	if result {
		tracing::debug!("deleting seed");
		delete_seed(&*db, jar, &target).await;
	}

	redirect
}

// Route to catch unauthenticated requests
#[rocket::get("/action/delete/<_target>", rank = 3)]
pub async fn anon(_target: String) -> Status { Status::Unauthorized }
