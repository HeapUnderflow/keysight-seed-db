// BSD 3-Clause License
//
// Copyright (c) 2020, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use crate::{
	db::Database,
	model::jwt_auth::AuthAdmin,
	page::api::RelRedirectTo,
	utils,
	utils::MessageType,
};
use rocket::{
	http::{CookieJar, Status},
	State,
};

#[rocket::get("/action/ban?<user>", rank = 1)]
pub async fn ban_user(
	_admin: AuthAdmin,
	jar: &CookieJar<'_>,
	db: State<'_, Database>,
	user: String,
) -> RelRedirectTo<'static> {
	let redirect = RelRedirectTo::new("../../banhammer");
	let rows_affected = match sqlx::query_file!("queries/ban_user.sql", user)
		.execute(&**db)
		.await
	{
		Ok(result) => result.rows_affected(),
		Err(why) => {
			tracing::error!(?why, "failed to ban user");
			utils::set_message_cookie(
				jar,
				MessageType::Error,
				"failed to ban user".to_owned(),
				true,
			);
			return redirect;
		},
	};

	if rows_affected == 0 {
		utils::set_message_cookie(
			jar,
			MessageType::Warn,
			"failed to ban user: not found".to_owned(),
			true,
		);
	}

	redirect
}

#[rocket::get("/action/ban", rank = 2)]
pub async fn ban_anon() -> Status { Status::Unauthorized }

#[rocket::get("/action/unban?<user>", rank = 1)]
pub async fn unban_user(
	_admin: AuthAdmin,
	jar: &CookieJar<'_>,
	db: State<'_, Database>,
	user: String,
) -> RelRedirectTo<'static> {
	let redirect = RelRedirectTo::new("../../banhammer");
	let rows_affected = match sqlx::query_file!("queries/unban_user.sql", user)
		.execute(&**db)
		.await
	{
		Ok(result) => result.rows_affected(),
		Err(why) => {
			tracing::error!(?why, "failed to ban user");
			utils::set_message_cookie(
				jar,
				MessageType::Error,
				"failed to ban user".to_owned(),
				true,
			);
			return redirect;
		},
	};

	if rows_affected == 0 {
		utils::set_message_cookie(
			jar,
			MessageType::Warn,
			"failed to unban user: not found".to_owned(),
			true,
		);
	}

	redirect
}

#[rocket::get("/action/unban", rank = 2)]
pub async fn unban_anon() -> Status { Status::Unauthorized }
