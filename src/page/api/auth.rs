// BSD 3-Clause License
//
// Copyright (c) 2020, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use crate::{
	config::Config,
	db::Database,
	model::{
		jwt_auth,
		jwt_auth::TwitchClaims,
		twitch::{TwitchToken, TwitchValidation},
		User,
		UserFlags,
		KEY,
	},
	page::api::{RelRedirectTo, AUTH_TOKEN_KEY, STATE_TOKEN_KEY},
	utils,
	utils::MessageType,
};
use bit_vec::BitVec;
use reqwest::{header, Client, Url};
use rocket::{
	http::{CookieJar, Status},
	response::Redirect,
	State,
};
use time::Duration;

#[allow(non_snake_case)]
pub fn ROUTES() -> Vec<rocket::Route> {
	rocket::routes![logout, login_twitch, code_twitch_success, code_twitch_error]
}

#[rocket::get("/logout")]
pub async fn logout(cookies: &CookieJar<'_>, config: State<'_, Config>) -> RelRedirectTo<'static> {
	cookies.remove(utils::make_cookie(
		AUTH_TOKEN_KEY,
		"",
		Duration::seconds(15),
		&config.twitch.base_url,
	));
	RelRedirectTo::new("../../")
}

#[rocket::get("/twitch/login")]
pub async fn login_twitch(config: State<'_, Config>, cookies: &CookieJar<'_>) -> Redirect {
	// state key. to be verified on the next request
	let state_key = crate::utils::random_alpha(64);

	let mut uri = Url::parse("https://id.twitch.tv/oauth2/authorize").expect("invalid static url");
	uri.query_pairs_mut()
		.append_pair("client_id", &config.twitch.client_id)
		.append_pair(
			"redirect_uri",
			&format!("{}/{}", &config.twitch.base_url, "api/twitch/code"),
		)
		.append_pair("response_type", "code")
		.append_pair("scope", "")
		.append_pair("state", &state_key)
		.append_pair("force_verify", "true");

	cookies.add(utils::make_cookie(
		STATE_TOKEN_KEY,
		state_key,
		time::Duration::days(1),
		&config.twitch.base_url,
	));
	Redirect::temporary(uri.as_str().to_owned())
}

macro_rules! code_str_err {
	($jar:expr, $m:expr) => {
		code_str_err!($jar, "../../../", $m);
	};
	($jar:expr, $p:expr, $m:expr) => {{
		crate::utils::set_message_cookie($jar, MessageType::Error, $m.to_owned(), true);
		Ok(RelRedirectTo::new($p))
	}};
}

#[allow(clippy::too_many_arguments)]
#[tracing::instrument(skip(jar, config, client, key, db, code, scope))]
#[rocket::get("/twitch/code?<code>&<scope>&<state>", rank = 1)]
pub async fn code_twitch_success(
	// cookies
	jar: &CookieJar<'_>,
	// managed states
	config: State<'_, Config>,
	client: State<'_, Client>,
	key: State<'_, KEY>,
	db: State<'_, Database>,
	// query args
	code: String,
	scope: String,
	state: String,
) -> Result<RelRedirectTo<'static>, Status> {
	match jar.get(STATE_TOKEN_KEY) {
		Some(flash) => {
			if flash.value() != state {
				return Err(Status::BadRequest);
			}
		},
		None => {
			tracing::info!("no state key");
			return Err(Status::PreconditionFailed);
		},
	};

	tracing::debug!("state key ok");
	tracing::debug!("handling login");
	// Fetch the actual auth token from twitch
	let token_response = client
		.post("https://id.twitch.tv/oauth2/token")
		.query(&[
			("client_id", config.twitch.client_id.as_str()),
			("client_secret", config.twitch.client_secret.as_str()),
			("code", code.as_str()),
			("grant_type", "authorization_code"),
			(
				"redirect_uri",
				&format!("{}/{}", &config.twitch.base_url, "api/twitch/code"),
			),
		])
		.send()
		.await;

	// Load the fetched response
	let data: TwitchToken = match token_response.and_then(|v| v.error_for_status()) {
		Ok(data) => match data.json().await {
			Ok(json) => json,
			Err(why) => {
				tracing::warn!(at=?"token_deserialize", ?why, "failed to load token from twitch");
				return code_str_err!(jar, "failed to load token from twitch".to_owned());
			},
		},
		Err(why) => {
			tracing::warn!(at=?"token_fetch", ?why, "failed to load response from twitch");
			return code_str_err!(jar, "failed to load response from twitch".to_owned());
		},
	};

	tracing::debug!(expires_in=%data.expires_in, "successfully received token");

	// Validate the response and get a user token object
	let validate_response = client
		.get("https://id.twitch.tv/oauth2/validate")
		.header(
			header::AUTHORIZATION,
			format!("Bearer {}", &data.access_token),
		)
		.send()
		.await;

	// Load the fetched user object
	let validate: TwitchValidation = match validate_response.and_then(|v| v.error_for_status()) {
		Ok(data) => match data.json().await {
			Ok(json) => json,
			Err(why) => {
				tracing::warn!(at=?"validate_deserialize", ?why, "failed to decode response from twitch");
				return code_str_err!(
					jar,
					"failed to decode validate response from twitch".to_owned()
				);
			},
		},
		Err(why) => {
			tracing::warn!(at=?"validate_deserialize", ?why, "failed to load response from twitch");
			return code_str_err!(
				jar,
				"failed to load validate response from twitch".to_owned()
			);
		},
	};

	tracing::debug!(uid=?validate.user_id, name=?validate.login, "validated");

	struct Row {
		id:    String,
		name:  String,
		image: Option<String>,
		flags: i16,
	}

	let query_result = sqlx::query_file!(
		"queries/create_user.sql",
		validate.user_id,
		validate.login,
		Option::<String>::None,
		BitVec::from_bytes(&UserFlags::default().bits().to_be_bytes()[..])
	)
	.fetch_one(&**db)
	.await;
	let user = match query_result {
		Ok(user) => User {
			id:    user.id.trim().to_owned(),
			name:  user.name.trim().to_owned(),
			image: None,
			flags: UserFlags::from(user.flags),
		},
		Err(why) => {
			tracing::error!(?why, "failed to create user in database");
			return code_str_err!(jar, "failed to register user in database".to_owned());
		},
	};

	if user.flags & UserFlags::BANNED == UserFlags::BANNED {
		tracing::error!(?user, "banned user attempted login");
		return code_str_err!(jar, "you are banned");
	}

	tracing::info!(?user, "logged in");

	let token = jwt_auth::generate_token(
		&*key,
		user.id,
		user.flags,
		Duration::days(7),
		TwitchClaims {
			img:  user.image,
			name: user.name,
		},
	);

	match token {
		Some(res) => {
			jar.add(utils::make_cookie(
				AUTH_TOKEN_KEY,
				res,
				time::Duration::days(7),
				&config.twitch.base_url,
			));
			Ok(RelRedirectTo::new("../../../"))
		},
		None => code_str_err!(jar, "failed to generate token"),
	}
}

#[rocket::get("/twitch/code?<error>&<error_description>&<state>", rank = 2)]
pub async fn code_twitch_error(
	jar: &CookieJar<'_>,
	error: String,
	error_description: String,
	state: String,
) -> RelRedirectTo<'static> {
	utils::set_message_cookie(
		jar,
		MessageType::Error,
		format!("{}: {}\nstate = {}", error, error_description, state),
		true,
	);
	RelRedirectTo::new("../../../")
}
