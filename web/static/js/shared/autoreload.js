/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2020, HeapUnderflow
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

class RlTimer {
    timer_seconds = null;
    timer_enabled = false;
    timer_current = null;
    _timer_count = 0;
    _tgt = null;

    _store_changed() {
        document.cookie = "js_reload_timer_enabled=" + (this.timer_enabled ? "yes" : "no");
        document.cookie = "js_reload_timer_s=" + this.timer_seconds;
    }

    change(enable) {
        this.timer_enabled = enable;
        this._store_changed()

        if (enable) {
            if (this.timer_current) {
                clearInterval(this.timer_current);
                this.timer_current = null;
            }

            this._timer_count = this.timer_seconds;

            if (this._tgt) { $(this._tgt)?.text(`AutoReload [${this._timer_count}s]`); }
            this.timer_current = setInterval(function(that) {
                if (that._timer_count > 0) {
                    that._timer_count--;
                    if (that._tgt) { $(that._tgt)?.text(`AutoReload [${that._timer_count}s]`); }
                } else {
                    window.location.reload();
                }
            }, 1000, this);
        } else if (this.timer_current) {
            $("#auto-reload-name").text("AutoReload");
            clearInterval(this.timer_current);
        }
    }

    set_target(tgt) {
        this._tgt = tgt;
    }

    set_time(sec) {
        this.timer_seconds = sec < 10 ? 10 : sec;
        this.change(this.timer_enabled);
    }

    init() {
        this.logger = new Logger(window.logger, "autoreload", Logger.INFO);

        this.logger.info("Starting init...");
        this.timer_enabled = getCookie("js_reload_timer_enabled", "no") === "yes";
        this.timer_seconds = parseInt(getCookie("js_reload_timer_s", "30"), 10);
        this.change(this.timer_enabled)
        this.logger.info("Completed: ", {enabled: this.timer_enabled, duration: this.timer_seconds, active: this.timer_current !== null});
    }
}

(function () {
    window.globals.autoreload = new RlTimer();
    window.globals.autoreload.init();
})();
