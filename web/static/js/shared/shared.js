/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2020, HeapUnderflow
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// Initialize globals if it didnt exist yet
if (!window.hasOwnProperty("globals")) {
    window.globals = {};
}

function getCookie(name, default_value = "") {
    const cname = name + "=";
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(cname) === 0) {
            return c.substring(cname.length, c.length);
        }
    }
    return default_value;
}

class Logger {
    constructor(parent, name, level) {
        this.root = parent ? parent.root : this;
        this.parent = parent;
        this.name = name;

        if ( this.root === this )
            this.captured_init = [];

        this.init = false;
        this.enabled = true;
        this.level = level || (parent && parent.level) || Logger.DEFAULT_LEVEL;

        this.children = {};
    }

    get(name, level) {
        if ( ! this.children[name] )
            this.children[name] = new Logger(this, (this.name ? `${this.name}.${name}` : name), level);

        return this.children[name];
    }

    debug(...args) {
        return this.invoke(Logger.DEBUG, args);
    }

    info(...args) {
        return this.invoke(Logger.INFO, args);
    }

    warn(...args) {
        return this.invoke(Logger.WARN, args);
    }

    warning(...args) {
        return this.invoke(Logger.WARN, args);
    }

    error(...args) {
        return this.invoke(Logger.ERROR, args);
    }

    /* eslint no-console: "off" */
    invoke(level, args) {
        if ( ! this.enabled || level < this.level )
            return;

        const message = Array.prototype.slice.call(args);

        if ( this.root.init )
            this.root.captured_init.push({
                time: Date.now(),
                category: this.name,
                message: message.join(' '),
                level: level
            });

        if ( this.name )
            message.unshift(`%cKSSDB [%c${this.name}%c]:%c`, 'color:#755000; font-weight:bold', '', 'color:#755000; font-weight:bold', '');
        else
            message.unshift('%cKSSDB:%c', 'color:#755000; font-weight:bold', '');

        if ( level === Logger.DEBUG )
            console.debug(...message);

        else if ( level === Logger.INFO )
            console.info(...message);

        else if ( level === Logger.WARN )
            console.warn(...message);

        else if ( level === Logger.ERROR )
            console.error(...message);

        else
            console.log(...message);
    }
}


Logger.DEFAULT_LEVEL = 2;

Logger.DEBUG = 1;
Logger.INFO = 2;
Logger.WARN = 4;
Logger.WARNING = 4;
Logger.ERROR = 8;
Logger.OFF = 99;

window.logger = new Logger(null, "kssdb", Logger.INFO)
