/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2020, HeapUnderflow
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

const HTML = '<link rel="stylesheet" href="static/css/bulma-dark.min.css" id="style-dark-mode">'

window.globals.darkmode = {}

class DarkMode {
    enabled = false;

    _dm(base) {
        let path = "static/css/bulma-dark.min.css";

        if (base) {
            path = base + "/" + path;
        }

        const link = document.createElement("link");
        link.setAttribute("rel", "stylesheet");
        link.setAttribute("href", path);
        link.setAttribute("id", "style-dark-mode");

        return link;
    }

    _store_changed() {
        document.cookie = "js_dark_mode=" + (this.enabled ? "yes" : "no") + ";expires=" + new Date(6969, 0, 1).toUTCString();
    }

    change(enable = false) {
        let $dm;

        if (document.getElementById("style-dark-mode")) {
            $dm = $("#style-dark-mode");
        } else {
            $dm = $(this._dm())
            $("body").prepend($dm);
        }

        $dm.prop("disabled", !enable);
        if (enable) {
            $("body").addClass("kssdb-dark-mode");
        } else {
            $("body").removeClass("kssdb-dark-mode");
        }
        this.enabled = enable
        this._store_changed()
    }

    init() {
        this.logger = new Logger(window.logger, "darkmode", Logger.INFO);

        this.logger.info("Starting init...");
        this.change(getCookie("js_dark_mode", "no") === "yes");
        this.logger.info("Completed: ", {enabled: this.enabled});
    }
}

(function () {
    window.globals.darkmode = new DarkMode();
    window.globals.darkmode.init();
})()
