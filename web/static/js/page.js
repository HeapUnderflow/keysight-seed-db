$(function () {
	// Check for click events on the navbar burger icon
	$(".navbar-burger").on("click", function () {
		// Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
		$(".navbar-burger").toggleClass("is-active");
		$(".navbar-menu").toggleClass("is-active");
	});

	// On clicking an image, enlarge it
	$(".modal-enlarge-image").on("click", function(event) {
		$("#img-modal-img").attr("src", event.target.src);
		$("#img-modal").addClass("is-active");
	});

	// When clicking the close button, close the modal
	$("div.modal button.kssdb-modal-close").on("click", function(event) {
		event.preventDefault();
		$(event.target).closest("div.modal").removeClass("is-active");
	});

	// Show the creation modal for the new-seed press
	$("#newseed").on("click", function(event) {
		event.preventDefault();
		showModal("#create-modal")
	});

	// Submit a new seed to the api
	$("#submit-new-seed").on("click", function(event) {
		event.preventDefault();
		const $newseed = $("#newseed-input");

		let ok = true;
		let text = $newseed.val().replace(/\s/g, '');
		console.log(text);
		if (!text || text.length < 3 || text.length > 512) {
			$newseed.addClass("is-danger")
			ok = false;
		} else {
			$newseed.removeClass("is-danger")
		}

		if (!ok) {
			return;
		}

		$(event.target).closest("form").trigger("submit");
	});

	// Open the search help modal
	$("#search-help").on("click", function() {
		showModal("#search-help-modal")
	})

	// Clear the search input
	$("#clear-text-query").on("click", function (e) {
		e.preventDefault();
		window.location.href = stripQueryParams(window.location.href);
		console.log(window.location);
	});

	// Fix thew clear button being triggered on [Enter]
	$("#text-query").on("keypress", function(event) {
		if (event.key === "Enter") {
			event.preventDefault();
			$("#form-search").trigger("submit");
		}
	});

	$(document).attr("data-do-delete", "")

	console.log("start complete");

	const $dm = $("#dark-mode");
	$dm.prop("checked", window.globals.darkmode.enabled);
	$dm.on("change", function(event) {
		const $target = $(event.target).prop("checked");
		window.globals.darkmode.change($target);
	});

	const $are = $("#auto-reload-enable");
	$are.prop("checked", window.globals.autoreload.timer_enabled);
	$are.on("change", function (event) {
		const $target = $(event.target).prop("checked");
		window.globals.autoreload.change($target);
	});

	const $arn = $("#auto-reload-num");
	$arn.val(window.globals.autoreload.timer_seconds);
	$arn.on("change keyup", function (event) {
		const $target = $(event.target).val();
		window.globals.autoreload.set_time($target);
	})

	window.globals.autoreload.set_target("#auto-reload-name");
});

function login() {
	showModal("#login-modal")
}

function showModal(modalId) {
	$(modalId).addClass("is-active")
}

function copy(id, key) {
	navigator.clipboard.writeText(`!seed ${key}`).then(() => {
		const $it = $(`${id}`);
		$it.removeClass("fa-copy");
		$it.addClass("fa-check");

		setTimeout(function() {
			let $it = $(`${id}`);
			$it.removeClass("fa-check");
			$it.addClass("fa-copy");
		}, 1500);
	})
}

function stripQueryParams(url) {
	const oldURL = url;
	let index;
	let newURL = oldURL;
	index = oldURL.indexOf('?');
	if(index === -1){
		index = oldURL.indexOf('#');
	}
	if(index !== -1){
		newURL = oldURL.substring(0, index);
	}

	return newURL;
}

// Base object for delete procedure
const seed_delete = {
	"data-attr": "data-do-delete",
	"start": function(id) {
		const $document = $(document);
		if ($document.attr(seed_delete["data-attr"])) {
			seed_delete.cancel($document.attr(seed_delete["data-attr"]));
		}

		const $normal = $(`#confirmdel-${id}`);
		if (!$normal) {
			return
		}

		$document.attr(seed_delete["data-attr"], id);

		$normal.children("#kssdb-one").css({"visibility": "hidden", "display": "none"});

		const $group = $("<div class='field is-grouped' id='kssdb-two'>");
		const $action_delete = $(`<div class='control'><a href='api/action/delete/${id}' class='button is-danger is-small'><span class='icon'><i class='fas fa-trash-alt'></i></span></a></div>`)
		const $action_cancle = $("<div class='control'><button class='button is-small'><span class='icon'><i class='fas fa-times'></i></span></button></div>")
		$action_cancle.on("click", () => seed_delete.cancel(id));
		$group.append($action_delete, $action_cancle);
		$normal.append($group);
	},
	"cancel": function (id) {
		const $document = $(document);
		$document.attr(seed_delete["data-attr"], null);
		const $normal = $(`#confirmdel-${id}`);
		if (!$normal) {
			return
		}

		$normal.children("#kssdb-two").remove();
		$normal.children("#kssdb-one").css({"visibility": "", "display": ""})
	},
};
