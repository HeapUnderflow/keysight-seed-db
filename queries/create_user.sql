-- Insert or Update the user with the login data
-- This will never change the id
-- noinspection SqlResolve
INSERT INTO users (id, name, image, flags)
    VALUES ($1, $2, $3, $4)
    ON CONFLICT
        ON CONSTRAINT pk_id
        DO UPDATE
        SET (name, image) = ($2, $3)
    RETURNING id, name, image, flags as "flags: BitVec";
