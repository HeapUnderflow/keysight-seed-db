-- noinspection SqlResolve
UPDATE users SET flags = set_bit(flags, 11, 1) WHERE name = $1 AND get_bit(flags, 7) != 1;
