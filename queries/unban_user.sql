-- noinspection SqlResolve
UPDATE users SET flags = set_bit(flags::bit(16), 11, 0) WHERE id = $1;
