-- noinspection SqlResolve
INSERT INTO seeds (id, user_id, created, image, tags) VALUES ($1, $2, DEFAULT, $3, $4);
