-- noinspection SqlResolve
SELECT seeds.id as seed,
       created,
       updated,
       seeds.image as seed_image,
       tags,
       users.id as user_id,
       name as user_name,
       users.image as user_image,
       flags as "flags: BitVec"
FROM seeds
LEFT JOIN users on users.id = seeds.user_id
ORDER BY updated DESC, created DESC;
