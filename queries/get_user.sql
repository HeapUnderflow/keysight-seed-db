-- noinspection SqlResolve
SELECT id, name, image, flags as "flags: BitVec"
FROM users WHERE id = $1;
