// BSD 3-Clause License
//
// Copyright (c) 2020, HeapUnderflow
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use std::{env, fs, path::Path, process::Command};

fn main() {
	let out_dir = env::var_os("OUT_DIR").unwrap();
	let dest_path = Path::new(&out_dir).join("constants.rs");

	let rev_output = Command::new("git")
		.args(&["rev-parse", "--short", "HEAD"])
		.output()
		.expect("failed to execute git");
	let rev = String::from_utf8(rev_output.stdout).expect("recieved invalid utf-8");

	fs::write(
		dest_path,
		format!(
			r#"
/// Compiled git-rev
pub const GIT_REV: &str = "{rev}";
/// Host compiled on
pub const HOST: &str = "{host}";
/// Target compiled for
pub const TARGET: &str = "{target}";
/// Profile used for compilation
pub const PROFILE: &str = "{profile}";
/// Is debug mode
pub const DEBUG_MODE: bool = {debug_mode};"#,
			rev = rev.trim(),
			host = env::var("HOST").unwrap(),
			target = env::var("TARGET").unwrap(),
			profile = env::var("PROFILE").unwrap(),
			debug_mode = env::var("PROFILE").map(|v| v != "release").unwrap_or(true)
		),
	)
	.expect("failed to write");
}
