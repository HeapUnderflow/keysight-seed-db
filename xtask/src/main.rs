use std::{env, process::Command};

type DynError = Box<dyn std::error::Error>;
fn main() {
	if let Err(e) = try_main() {
		eprintln!("{}", e);
		std::process::exit(-1);
	}
}

fn try_main() -> Result<(), DynError> {
	match env::args()
		.nth(1)
		.unwrap_or_else(String::new)
		.to_ascii_lowercase()
		.as_str()
	{
		"perm" => {
			perm();
			Ok(())
		},
		"init" => init(),
		"-h" | "--help" => {
			help();
			Ok(())
		},
		other => {
			help();
			eprintln!("error: unknown command {}", other);
			Ok(())
		},
	}
}

fn help() {
	eprintln!(
		"xtask v{}\n\nperm\t\tgenerate permission value",
		env!("CARGO_PKG_VERSION")
	)
}

fn perm() {
	let mut out = 0u16;
	for arg in env::args().skip(2) {
		match arg.to_ascii_lowercase().as_str() {
			"user" => out |= 0b0000_0000_0000_0001,
			"admin" => out |= 0b0000_0001_0000_0000,
			"owner" => out |= 0b1000_0000_0000_0000,
			"banned" => out |= 0b0000_0000_0001_0000,
			p => {
				eprintln!("unknown permission '{}'", p);
				return;
			},
		}
	}
	println!("perm(u16) = {:16}", out);
	println!("perm(i16) = {:16}", out as i16);
	println!("perm(bin) = {:016b}", out);
}

fn init() -> Result<(), DynError> {
	eprintln!("migrating database...");
	sqlx_migrate()?;

	eprintln!("init ok");
	Ok(())
}

fn sqlx_migrate() -> Result<(), DynError> {
	Command::new("sqlx")
		.args(&["migrate", "run"])
		.spawn()
		.map(|mut v| v.wait())??;
	Ok(())
}
