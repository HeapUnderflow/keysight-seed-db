<a name="0.2.2"></a>
## 0.2.2 (unofficial) Keysight Seed DB (2021-02-07)


#### Bug Fixes

*   fix tokio runtime builder ([acd64881](https://gitlab.com/HeapUnderflow/keysight-seed-db/commit/acd64881f3be6b5af3399d382f657dab850decc0))
* **AutoReload:**  disallow timer below 10 seconds ([8da8f879](https://gitlab.com/HeapUnderflow/keysight-seed-db/commit/8da8f879546a9506e4dbff4001ed9973d33e835b))
* **queries:**  use correct ordering for seeds ([a7c12e42](https://gitlab.com/HeapUnderflow/keysight-seed-db/commit/a7c12e42f122ca5422834ac6c0bff22a44971364))

#### Features

*   start page serving and ui ([91b042fd](https://gitlab.com/HeapUnderflow/keysight-seed-db/commit/91b042fdd54c2f28a9b570f4b4b2165db5b92dac))
*   finish edit api endpoint ([50638c30](https://gitlab.com/HeapUnderflow/keysight-seed-db/commit/50638c30d59aa97530241b007865e731bcf8079f))
* **api/action/edit:**  implement edit endpoint ([946a91d9](https://gitlab.com/HeapUnderflow/keysight-seed-db/commit/946a91d96e1abd85f9a2540a9bd0b04dcdcf1e04))
* **database:**  add updated column ([55b669f2](https://gitlab.com/HeapUnderflow/keysight-seed-db/commit/55b669f2ddba802a590f1b5f3b16caf6a71ba289))
* **edit:**
  *  add edit feature and bump version to 0.2.2 ([2f8f5b59](https://gitlab.com/HeapUnderflow/keysight-seed-db/commit/2f8f5b5909598242378825e53a6440ccbb65da67))
  *  add back button to edit page ([20e75fde](https://gitlab.com/HeapUnderflow/keysight-seed-db/commit/20e75fde294b2e8d74c9bb4e540fdfc5e7a34e80))
  *  add edit option to seed page ([50931b0c](https://gitlab.com/HeapUnderflow/keysight-seed-db/commit/50931b0c79fca989c17c5c3de6bc916a5427c65b))
* **messages:**  add typed messages ([94e85238](https://gitlab.com/HeapUnderflow/keysight-seed-db/commit/94e852387e971a552596de471d36d634a5f89f7f))
* **model/seed:**  add proper FromRow implementation for Seed ([a43799cc](https://gitlab.com/HeapUnderflow/keysight-seed-db/commit/a43799cc565e638ec5008c998e1cda8ea3699860))
* **redirects:**  allow relredirects to be constructed with both borrowed and owned strings ([28cd2db3](https://gitlab.com/HeapUnderflow/keysight-seed-db/commit/28cd2db3b2338f0a607c6a334cad5ed76838a05c))



<a name="0.2.1"></a>
## 0.2.1 (unofficial) Keysight Seed DB (2021-01-04)


#### Features

* **messages:**  add typed messages ([94e85238](https://gitlab.com/HeapUnderflow/keysight-seed-db/commit/94e852387e971a552596de471d36d634a5f89f7f))

#### Bug Fixes

* **AutoReload:**  disallow timer below 10 seconds ([8da8f879](https://gitlab.com/HeapUnderflow/keysight-seed-db/commit/8da8f879546a9506e4dbff4001ed9973d33e835b))



